// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  domain: '23.97.173.253/',
  service: {
    users: 'oxygen/',
    subscriptions: 'hydrogen/',
    posts: 'articles/',
    auth: 'oxygen/',
  },
  actions: {
    //omni
    health: { url: 'health', method: 'GET' },
    //hydrogen
    new_post: { service: 'subscriptions', url: 'new_post', method: 'POST' },
    subscribe: {
      service: 'subscriptions',
      url: 'subscribe',
      method: 'POST',
      params: ['id'],
    },
    unsubscribe: {
      service: 'subscriptions',
      url: 'subscribe/',
      method: 'DELETE',
      params: ['id'],
    },
    followers: {
      service: 'subscriptions',
      url: 'followers/',
      method: 'GET',
      params: ['user_id'],
    },
    subscriptions: {
      service: 'subscriptions',
      url: 'subscriptions/',
      method: 'GET',
      params: ['user_id'],
    },
    my_distance: {
      service: 'subscriptions',
      url: 'my_distance/',
      method: 'GET',
      params: ['watched_id'],
    },
    //oxygen
    users: { service: 'users', url: 'users', method: 'GET', params: ['q'] },
    register: {
      service: 'users',
      url: 'users',
      method: 'POST',
      params: ['displayName', 'email', 'password'],
    },
    getuser: {
      service: 'users',
      url: 'users/',
      method: 'GET',
      params: ['id']
    },
    editusername: {
      service: 'users',
      url: 'users', 
      method: 'PUT',
      params: ['display_name']
    },
    edituserpass: {
      service: 'users',
      url: 'users/change_password',
      method: 'PUT',
      params: ['password']
    },
    deleteprofile: {
      service: 'users',
      url: 'users',
      method: 'DELETE',
      params: []
    },
    //articles
    post: {
      service: 'posts',
      url: 'article',
      method: 'POST',
      params: ['content', 'attachments'],
    },
    comment: {
      service: 'posts',
      url: 'article/',
      method: 'POST',
      params: ['post_id', 'content', 'attachments'],
    },
    getpost: {
      service: 'posts',
      url: 'article/',
      method: 'GET',
      params: ['post_id'],
    },
    getposts: {
      service: 'posts',
      url: 'article',
      method: 'GET',
      params: ['user_id', 'since', 'to'],
    },
    updatepost: {
      service: 'posts',
      url: 'article/',
      method: 'PUT',
      params: ['post_id'],
    },
    deletepost: {
      service: 'posts',
      url: 'article/',
      method: 'DELETE',
      params: ['post_id'],
    },
    //oxygen auth
    login: {
      service: 'users',
      url: 'auth',
      method: 'POST',
      params: ['email,password'],
    },
  },
  protocol: 'http://', //"https://"
};
