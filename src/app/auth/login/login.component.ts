import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';

import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() { }
  public loginFailed = false;
  onLogin(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;

    this.authService.sendLogin(email, password).subscribe(response => {
      const userToken = response['access_token'];
      localStorage.setItem('authToken', userToken);
      this.router.navigate(['/']);
      console.log('got token', userToken);
    }
      , e => this.loginFailed = true
    );
  }
}
