import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) {}

  public _email: string;
  public email = new Subject<string>();
  public displayName: string;
  public _password: string;
  public password = new Subject<string>();
  public _rep_password;
  string;
  public rep_password = new Subject<string>();
  public passwordsMatch = true;
  public passwordComplexEnough = true;
  public emailValid = true;

  ngOnInit() {
    this.email
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(s => {
        this._email = s;
        this.validateMail();
      });
    this.password
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(s => {
        this._password = s;
        this.validatePaswords();
      });
    this.rep_password
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(s => {
        this._rep_password = s;
        this.validatePaswords();
      });
  }

  validatePaswords() {
    let re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/;
    this.passwordComplexEnough = re.test(this._password);
    this.passwordsMatch =
      this._password &&
      this._rep_password &&
      this._password == this._rep_password;
  }
  validateMail() {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.emailValid = !this._email
      ? !!this._email
      : re.test(String(this._email).toLowerCase());
  }

  onSignup(form: NgForm) {
    this.validateMail();
    this.validatePaswords();
    if (this.emailValid && this.passwordsMatch && this.passwordComplexEnough)
      this.authService
        .sendSignup(this._email, this._password, this.displayName)
        .flatMap(response =>
          this.authService.sendLogin(this._email, this._password),
        )
        .subscribe(response => {
          const userToken = response['access_token'];
          localStorage.setItem('authToken', userToken);
          this.router.navigate(['/']);
          console.log('got token', userToken);
        });
  }
}
