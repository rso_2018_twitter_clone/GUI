import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

import { HttpService } from '../shared/http.service';

@Injectable()
export class AuthService {
  constructor(
    private httpClient: HttpClient,
    public jwtHelper: JwtHelperService,
  ) {}

  isAuthenticated() {
    const promise = new Promise((resolve, reject) => {
      resolve(this.hasAuthToken());
    });
    return promise;
  }

  public hasAuthToken() {
    const authToken = localStorage.getItem('authToken');
    if (!authToken) {
      console.log('TOKEN IS EMPTY');
      return false;
    }

    const expired = this.jwtHelper.isTokenExpired(authToken);
    if (expired) {
      console.log('TOKEN EXPIRED');
      return false;
    }
    return true;
  }

  sendSignup(emailVar: string, passwordVar: string, displayNameVar: string) {
    return this.httpClient.post(`${HttpService.apiUrl}/oxygen/users`, {
      email: emailVar,
      password: passwordVar,
      display_name: displayNameVar,
    });
  }

  sendLogin(emailVar: string, passwordVar: string) {
    return this.httpClient.post(`${HttpService.apiUrl}/oxygen/auth`, {
      email: emailVar,
      password: passwordVar,
    });
  }

  public loggedUserId() {
    const { identity } = this.jwtHelper.decodeToken(
      localStorage.getItem('authToken'),
    );
    // console.log('logged as', identity);
    return identity;
  }

  public logOut() {
    console.log('logOut()');
    localStorage.setItem('authToken', '');
  }
}
