import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class FabVisibilityService {
  private fabVisibilitySource = new BehaviorSubject<boolean>(true);
  public onUserProfile: boolean = false;

  currentFabVisibility = this.fabVisibilitySource.asObservable();

  private reloadPosts = new BehaviorSubject<number>(0);
  reloadPosts$ = this.reloadPosts.asObservable();

  constructor() {}

  changeFabVisibility(fabVisibility: boolean) {
    this.fabVisibilitySource.next(fabVisibility);
    //console.log("changeFabVisibility(): " + this.currentFabVisibility);
  }

  forceReload(id = -1) {
    this.reloadPosts.next(id);
  }
}
