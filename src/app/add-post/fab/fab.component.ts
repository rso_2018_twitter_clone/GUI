import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-fab',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.css'],
})
export class FabComponent implements OnInit {
  isFabActive: boolean = false;

  @Output() eventEmitter = new EventEmitter<boolean>();

  constructor(public authService: AuthService) {}

  ngOnInit() {}

  onClickFab() {
    this.isFabActive = !this.isFabActive;
    this.eventEmitter.emit(this.isFabActive);
    //console.log("FabComponent.isFabActive: " + this.isFabActive);
  }
}
