import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FabVisibilityService } from './fab-visibility.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css'],
})
export class AddPostComponent implements OnInit {
  fabStatus: boolean;
  fabVisibility: boolean;

  constructor(private fabVisibilityService: FabVisibilityService) {}

  ngOnInit() {
    this.fabVisibilityService.currentFabVisibility.subscribe(fabVisibility => {
      this.fabVisibility = fabVisibility;
      //console.log("ngOnInit(): " + this.fabVisibility);
    });
  }

  receiveFabStatus(s) {
    console.log('FAB STATUSSSS', s);
    this.fabStatus = s;
    //console.log("fabStatus: " + this.fabStatus);
  }
}
