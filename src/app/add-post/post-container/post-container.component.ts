import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from '../../shared/http.service';
import { AuthService } from '../../auth/auth.service';
import { FabVisibilityService } from './../fab-visibility.service';

@Component({
  selector: 'app-post-container',
  templateUrl: './post-container.component.html',
  styleUrls: ['./post-container.component.css'],
})
export class PostContainerComponent implements OnInit {
  constructor(
    private router: Router,
    private http: HttpService,
    private fabVisibilityService: FabVisibilityService,
    private authService: AuthService,
  ) {}

  ngOnInit() {}

  @Output() eventEmitter = new EventEmitter<boolean>();

  public attachments = [];
  public text = '';

  filesSelected(event) {
    if ((event.target.files && event.target.files.length > 0) || null) {
      this.attachments = [];
      for (let i = 0; i < event.target.files.length; i++) {
        this.getBase64(event.target.files.item(i));
      }
    }
  }

  removeAttachment(item) {
    this.attachments = this.attachments.filter(x => x != item);
  }

  getBase64(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    console.log(reader.result);
    let att = this.attachments;
    reader.onload = function() {
      let fileatt = { file: file, base64: reader.result };
      att.push(fileatt);
    };
    reader.onerror = function(error) {
      console.log('Error: ', error);
    };
  }

  addComment() {
    let postBody = {
      attachments: this.attachments.map(x => x.base64),
      content: this.text,
    };
    this.http.sendRequest('post', '', postBody).subscribe(
      () => {
        if(this.fabVisibilityService.onUserProfile != true) {
          this.fabVisibilityService.forceReload(this.authService.loggedUserId());
        }
      },
      () => {
        this.eventEmitter.emit(false);
      },
    );
  }

  onLogin(x) {} // TODO(Michal) ??? this is called in template
}
