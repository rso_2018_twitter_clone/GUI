import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService } from './auth/auth-guard.service';

import { BodyComponent } from './body/body.component';
import { SignupComponent } from './auth/signup/signup.component';
import { LoginComponent } from './auth/login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { SettingsComponent } from './settings/settings.component';
import { CanDeactivateGuard } from './settings/can-deactivate-guard.service';

const appRoutes: Routes = [
  { path: '', canActivate: [AuthGuardService], component: BodyComponent },
  { path: 'user/:id', component: BodyComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'welcome', component: WelcomeComponent },
  {
    path: 'settings',
    canActivate: [AuthGuardService],
    component: SettingsComponent,
    canDeactivate: [CanDeactivateGuard],
  },
  { path: '**', canActivate: [AuthGuardService], component: BodyComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
