import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { SubscriptionsService } from './subscriptions/subscriptions.service';
import { FabVisibilityService } from './add-post/fab-visibility.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'app';
  fabVisibility: boolean;

  constructor(
    private fabVisibilityService: FabVisibilityService,
    private subscriptionsService: SubscriptionsService,
    private toastrService: ToastrService,
    private router: Router,
  ) {
    subscriptionsService.login();
    subscriptionsService.notifications$.subscribe(({ user_id }) => {
      this.toastrService.info(
        `<a href="/user/${user_id}">One of your subscriptions has published new post.</a>`,
        'New post',
        {
          disableTimeOut: true,
          enableHtml: true,
        },
      );
    });
  }

  ngOnInit() {
    this.fabVisibilityService.currentFabVisibility.subscribe(fabVisibility => {
      this.fabVisibility = fabVisibility;
      //console.log("ngOnInit(): " + this.fabVisibility);
    });
  }

  changeOfRoutes() {
    this.fabVisibility = (this.router.url == '/' || this.router.url.includes('/user/'));
    this.fabVisibilityService.onUserProfile = this.router.url.includes('/user/');
    this.fabVisibilityService.changeFabVisibility(this.fabVisibility);
    //console.log("changeOfRoutes(): " + this.fabVisibility);
  }
}
