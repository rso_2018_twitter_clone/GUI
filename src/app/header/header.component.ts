import { Component, OnInit } from '@angular/core';
import { HttpService } from '../shared/http.service';
import { AuthService } from '../auth/auth.service';
import { BehaviorSubject, Subject } from "rxjs"
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  host: { '(document:click)': 'clearUsers()', }
})
export class HeaderComponent implements OnInit {

  _findString: string;
  findString = new Subject<string>();
  foundUsers = [];
  user;
  constructor(
    private httpService: HttpService,
    public authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,

  ) { }
  hasDetails: boolean = false

  ngOnInit() {
    this.findString.debounceTime(500).distinctUntilChanged().subscribe(s => { this._findString = s; this.findUsers() });
    this.router.events.subscribe(x => { this.hasDetails = x instanceof NavigationEnd && x.url.includes("user"); })
    this.user = this.authService.loggedUserId();
  }
  findUsers() {
    this.httpService.sendRequest("users", "?" + this._findString).subscribe((s: any[]) => this.foundUsers = s.filter(x => x.display_name.includes(this._findString)).slice(0, 4));
  }
  clearUsers() {
    this.foundUsers = [];
  }
  logout() {
    console.log('logout()');
    this.authService.logOut();
    this.router.navigate(['/welcome']);
  }

}
