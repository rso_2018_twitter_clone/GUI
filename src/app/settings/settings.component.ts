import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';

import { CanComponentDeactivate } from './can-deactivate-guard.service';
import { HttpService } from '../shared/http.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit, CanComponentDeactivate {
  changesSaved: boolean = false;
  public user$;
  userDisplay_name: string;

  constructor(private router: Router, private http: HttpService, private authService: AuthService) { }

  ngOnInit() {
    this.password
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(s => {
        this._password = s;
        this.validatePaswords();
      });
    this.rep_password
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(s => {
        this._rep_password = s;
        this.validatePaswords();
      });

    this.user$ = this.http.sendRequest('getuser', this.authService.loggedUserId()).subscribe(
      (s: any[]) => {
        this.userDisplay_name = s["display_name"];
        console.log(this.userDisplay_name);
      });
  }

  onSaveSettingsDispName(form: NgForm) {
    const displayNameConst = this.displayName;
    const notificationsConst = this.notify;

    this.http.sendRequest('editusername', '', { display_name: this.displayName }).subscribe(
      () => {
        console.log('username edited');
        this.router.navigate(['/']);
      }
    );


  }

  onSaveSettingsPass(form: NgForm) {
    this.validatePaswords();
    const passwordConst = this._password;

    this.http.sendRequest('edituserpass', '', {password:this._password}).subscribe(
      () => {
        console.log('password edited');
        this.router.navigate(['/']);
      }
    );


  }

  public notify: boolean;
  public displayName: string;
  public _password: string;
  public password = new Subject<string>();
  public _rep_password;
  string;
  public rep_password = new Subject<string>();
  public passwordsMatch = true;
  public passwordComplexEnough = true;

  validatePaswords() {
    let re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/;
    this.passwordComplexEnough = re.test(this._password);
    this.passwordsMatch =
      this._password &&
      this._rep_password &&
      this._password == this._rep_password;
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate()');
    return true;
  }

  removeProfile() {
    if(confirm("Are you sure? This change is irreversible."))
    //TODO

    this.http.sendRequest('deleteprofile').subscribe(x => {
      console.log('Profile deleted');
      this.authService.logOut();
      this.router.navigate(['/welcome']);
    });



  }
}
