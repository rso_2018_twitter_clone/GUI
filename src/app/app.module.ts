import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { CanDeactivate } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CanDeactivateGuard } from './settings/can-deactivate-guard.service';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { ToastrModule } from 'ngx-toastr';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { HttpService } from './shared/http.service';
import { AuthService } from './auth/auth.service';
import { AuthGuardService } from './auth/auth-guard.service';
import { SessionStorageService } from './shared/session-storage.service';
import { FabVisibilityService } from './add-post/fab-visibility.service';
import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { BodyComponent } from './body/body.component';
import { HeaderComponent } from './header/header.component';
import { MainContainerComponent } from './body/main-container/main-container.component';
import { AuxiliaryContainerComponent } from './body/auxiliary-container/auxiliary-container.component';
import { UserDetailsComponent } from './body/auxiliary-container/user-details/user-details.component';
import { TweetListComponent } from './body/main-container/tweet-list/tweet-list.component';
import { TweetItemComponent } from './body/main-container/tweet-list/tweet-item.component';
import { SignupComponent } from './auth/signup/signup.component';
import { LoginComponent } from './auth/login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AddPostComponent } from './add-post/add-post.component';
import { FabComponent } from './add-post/fab/fab.component';
import { PostContainerComponent } from './add-post/post-container/post-container.component';
import { SettingsComponent } from './settings/settings.component';
import { SubscriptionsService } from './subscriptions/subscriptions.service';

const socketConfig: SocketIoConfig = {
  url: `rsodocker.cloudapp.net:7004`,
  options: {},
};

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    BodyComponent,
    HeaderComponent,
    MainContainerComponent,
    AuxiliaryContainerComponent,
    UserDetailsComponent,
    TweetListComponent,
    TweetItemComponent,
    SignupComponent,
    LoginComponent,
    WelcomeComponent,
    AddPostComponent,
    FabComponent,
    PostContainerComponent,
    SettingsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    SocketIoModule.forRoot(socketConfig),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    AngularFontAwesomeModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
      },
    }),
  ],
  providers: [
    HttpService,
    AuthService,
    AuthGuardService,
    CanDeactivateGuard,
    SessionStorageService,
    FabVisibilityService,
    SubscriptionsService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
