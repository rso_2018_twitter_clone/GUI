import { Injectable } from '@angular/core';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

import { AuthService } from '../auth/auth.service';

@Injectable()
export class SubscriptionsService {
  notifications$ = new Subject();

  constructor(private authService: AuthService, private socket: Socket) {
    const s = this.socket
      .fromEvent<{ post_id: number; user_id: number }>('post')
      .map(({ user_id, post_id }) => ({ user_id, post_id }))
      .subscribe(this.notifications$);
    this.notifications$.subscribe(console.log);
  }

  login() {
    this.authService.isAuthenticated().then(hasToken => {
      if (hasToken) {
        this.socket.emit('login', {
          access_token: localStorage.authToken,
        });
      }
    });
  }
}
