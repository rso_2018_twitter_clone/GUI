import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-auxiliary-container',
  templateUrl: './auxiliary-container.component.html',
  styleUrls: ['./auxiliary-container.component.css'],
})
export class AuxiliaryContainerComponent {
  constructor(public route: ActivatedRoute) {}
}
