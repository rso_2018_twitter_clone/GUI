import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { HttpService } from '../../../shared/http.service';
import { AuthService } from '../../../auth/auth.service';
import { BehaviorSubject } from 'rxjs';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css'],
})
export class UserDetailsComponent {
  @Input()
  public set userId(newId: string) {
    this.fetchUser(newId);
    this.fetchFollowers(newId);
    this.fetchSubscriptions(newId);
    this.resolveIsSubbed(newId);
  }

  public isSubscribed;
  public user$;
  public followers$;
  public subscriptions$;

  constructor(
    private httpService: HttpService,
    public authService: AuthService,
  ) {
    this.authService.loggedUserId();
  }

  toggleSub(userId) {
    let action = this.isSubscribed?'unsubscribe':'subscribe';
    let query = this.isSubscribed?userId:''
    return this.httpService
      .sendRequest(action, query, { subscribed: userId })
      .subscribe(v => {
        this.fetchFollowers(userId);
        this.resolveIsSubbed(userId);
      });
  }

  fetchFollowers(userId) {
    this.followers$ = this.httpService
      .sendRequest('followers', userId)
      .catch(err => of({ followers: [] }))
      .map(data => data['followers']);
  }

  fetchSubscriptions(userId) {
    this.subscriptions$ = this.httpService
      .sendRequest('subscriptions', userId)
      .catch(err => of({ subscriptions: [] }))
      .map(data => data['subscriptions']);
  }

  fetchUser(userId) {
    this.user$ = this.httpService.sendRequest('getuser', userId);
  }
  resolveIsSubbed(userId) {
    this.httpService.sendRequest('my_distance', userId).subscribe(x => this.isSubscribed = x["path_length"] == 1)
  }
}
