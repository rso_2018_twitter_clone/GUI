import { Component, OnInit, Input } from '@angular/core';

import { Tweet } from '../../../models/tweet.model';
import { Comment } from '../../../models/comment.model';
import { toUnicode } from 'punycode';
import { SessionStorageService } from '../../../shared/session-storage.service';
import { HttpService } from '../../../shared/http.service';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'app-tweet-item',
  templateUrl: './tweet-item.component.html',
  styleUrls: ['./tweet-item.component.css'],
})
export class TweetItemComponent {
  constructor(private sss: SessionStorageService, private http: HttpService, private authService: AuthService) { }

  public user$;

  getTweetUserInfo(t) {
    this.user$ = this.http.sendRequest('getuser', t.UserId);
  }

  public tweet;
  public editableTweet = {
    isOpen: false,
    text: '',
    attachments: [],
  };

  @Input('tweet')
  public set stweet(t) {
    this.tweet = t;
    if (t) {
      this.getTweetUserInfo(t);
    }
  }
  comment = {
    isOpen: false,
    text: '',
    attachments: [],
  };

  filesSelected(event) {
    if ((event.target.files && event.target.files.length > 0) || null) {
      this.comment.attachments = [];
      for (let i = 0; i < event.target.files.length; i++) { this.getBase64(event.target.files.item(i), this.comment.attachments); }

    }
  }
  editFilesSelected(event) {
    if ((event.target.files && event.target.files.length > 0) || null) {
      // this.editableTweet.attachments = [];
      for (let i = 0; i < event.target.files.length; i++) { this.getBase64(event.target.files.item(i), this.editableTweet.attachments); }

    }
  }
  removeAttachment(item) {
    this.comment.attachments = this.comment.attachments.filter(x => x != item);
  }
  removeEditAttachment(item) {
    this.editableTweet.attachments = this.editableTweet.attachments.filter(x => x != item);
  }

  getBase64(file, attachments) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    console.log(reader.result);
    let att = attachments;
    reader.onload = function () {
      let fileatt = { "file": file, "base64": reader.result }
      att.push(fileatt);
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }
  addComment() {
    let postBody = {
      content: this.comment.text,
      attachments: this.comment.attachments.map(x => x.base64),
    };
    // console.log(this.comment.attachments.map(x => x.base64));

    this.http
      .sendRequest('comment', this.tweet.PostId, postBody)
      .subscribe(x => {
        this.comment.isOpen = false;
        this.http
          .sendRequest('getpost', this.tweet.PostId)
          .subscribe(y => (this.tweet = y));
      });
  }

  generateDownload(item) {
    let hash = 0,
      i,
      chr;

    for (i = 0; i < item.Attachment.length; i++) {
      chr = item.Attachment.charCodeAt(i);
      hash = (hash << 5) - hash + chr;
      hash |= 0; // Convert to 32bit integer
    }
    let index = item.Attachment.indexOf(';');
    let configString: string = item.Attachment.slice(0, index);
    let extension = configString.split('/')[1];
    return `attachment ${hash}.${extension}`;
  }
  closeComment() {
    this.comment = { isOpen: false, text: "", attachments: [] };
  }


  canComment() {
    let now = (new Date()).getTime();
    let posttime = (new Date(this.tweet.PostDate)).getTime();
    let timeElapsed = now - posttime;
    return this.tweet.UserId == this.authService.loggedUserId() && timeElapsed <= 600000 && this.tweet.Comments.length < 1;
  }
  openEdit() {
    this.editableTweet.isOpen = true;
    this.editableTweet.text = this.tweet.Message
    this.editableTweet.attachments = [];
    this.tweet.attachments.forEach(x => this.editableTweet.attachments.push({ base64: x.Attachment }));
  }
  saveEdit() {
    let postBody = {
      content: this.editableTweet.text,
      attachments: this.editableTweet.attachments.map(x => x.base64),
    };
    this.http
      .sendRequest('updatepost', this.tweet.PostId, postBody)
      .subscribe(x => {
        this.cancelEdit();
        this.http
          .sendRequest('getpost', this.tweet.PostId)
          .subscribe(y => (this.tweet = y));
      });
  }
  cancelEdit() {
    this.editableTweet.isOpen = false;
    this.editableTweet.text = "";
    this.editableTweet.attachments = [];
    this.comment = { isOpen: false, text: '', attachments: [] };
  }
  removePost() {
    if(confirm("Are you sure?"))
    this.http
      .sendRequest('deletepost', this.tweet.PostId)
      .subscribe(x => {
        this.tweet.isDeleted = true;
      });
  }
}
