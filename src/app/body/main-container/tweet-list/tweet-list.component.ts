import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Tweet } from '../../../models/tweet.model';
import { Comment } from '../../../models/comment.model';
import { HttpService } from '../../../shared/http.service';
import { FabVisibilityService } from '../../../add-post/fab-visibility.service';

@Component({
  selector: 'app-tweet-list',
  templateUrl: './tweet-list.component.html',
  styleUrls: ['./tweet-list.component.css'],
})
export class TweetListComponent implements OnInit {
  constructor(
    private http: HttpService,
    public route: ActivatedRoute,
    private fabVisibilityService: FabVisibilityService,
  ) {}

  public tweets$;

  ngOnInit() {
    this.route.params.subscribe(({ id }) => {
      const query = id ? `?user_id=${id}` : undefined;
      this.fetchTweets(query);
    });
    this.fabVisibilityService.reloadPosts$.subscribe(id => {
      if (id === -1) {
        this.fetchTweets(undefined);
      } else if (id > 0) {
        this.fetchTweets(`?user_id=${id}`);
      }
    });
  }

  fetchTweets(query) {
    const users = {};
    this.tweets$ = this.http
      .sendRequest('getposts', query)
      .map((tweets: any[]) =>
        tweets.map(({ UserId, ...rest }) => {
          let cached = users[UserId];
          if (!cached) {
            cached = users[UserId] = this.http
              .sendRequest('getuser', UserId)
              .share();
          }

          return { user$: cached, UserId, ...rest };
        }),
      );
  }
}
