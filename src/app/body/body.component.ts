import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css'],
})
export class BodyComponent implements OnInit {
  hasDetails: boolean = false;
  fabVisibility: boolean = false;

  constructor(public route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe(x => (this.hasDetails = !!x.id));
  }
}
