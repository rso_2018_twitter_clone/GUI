import { Comment } from './comment.model';

export class Tweet {
  public authorFirstName: string;
  public authorLastName: string;
  public date: string;
  public content: string;
  public imgPath: string;
  public comments: Comment[];

  constructor(firstName: string, lastName: string, date: string, content: string, imgPath: string, comments: Comment[]) {
    this.authorFirstName = firstName;
    this.authorLastName = lastName;
    this.date = date;
    this.content = content;
    this.imgPath = imgPath;
    this.comments = [...comments];
  }

}
