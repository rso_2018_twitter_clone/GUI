export class Comment {
  public authorFirstName: string;
  public authorLastName: string;
  public date: string;
  public content: string;
  public imgPath: string;

  constructor(firstName: string, lastName: string, date: string, content: string, imgPath: string) {
    this.authorFirstName = firstName;
    this.authorLastName = lastName;
    this.date = date;
    this.content = content;
    this.imgPath = imgPath;
  }
}
