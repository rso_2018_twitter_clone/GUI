import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class HttpService {
  public static readonly apiUrl: string = 'http://23.97.173.253';
  constructor(private httpClient: HttpClient) {
    this.sendRequest('users').subscribe();
  }

  sendSearchText(UserDetails: string) {
    var userDetUrl = `${HttpService.apiUrl}/oxygen/users?q=${UserDetails}`;

    console.log(
      'sendSearchText().userDet -- URL to search user: ' + userDetUrl,
    );
    return this.httpClient.get(userDetUrl);
  }

  //{"email": "asd", "password": "pass", "display_name": "a s d"}

  // prepareData(params, action) {
  //   if (action.method === "GET") {
  //     let qs = "?";
  //     let paramArray = [];
  //     for (let i = 0; i < action.params.length; i++) {
  //       let key = action.params[i];
  //       let val = params[key];
  //       if (Array.isArray(val)) val.forEach(x => paramArray.push(key + "=" + x));
  //       else paramArray.push(key + "=" + key);
  //     }
  //     return qs += paramArray.join("&");
  //   }
  //   else {
  //     let paramDict = {};
  //     action.params.forEach(x => paramDict[x] = params[x]);
  //     return paramDict;
  //   }
  // }

  sendRequest(actionName, query = '', body = null) {
    // console.log(actionName);
    let action = environment.actions[actionName];
    // console.log(environment.protocol);
    // console.log(environment.service[action.service]);
    // console.log(action.url);
    // console.log(query ? query : '');
    let url =
      environment.protocol +
      environment.domain +
      environment.service[action.service] +
      action.url +
      query;
    // let data = this.prepareData(params, environment.actions[action]);

    const headers = {};
    const token = localStorage.authToken;
    if (token) {
      headers['Authorization'] = `JWT ${token}`;
    }
    // console.log(headers);
    // console.log(url);
    switch (action.method) {
      case 'GET':
        return this.httpClient.get(url, { headers });
      case 'POST':
        return this.httpClient.post(url, body, { headers });
      case 'PUT':
        return this.httpClient.put(url, body, { headers });
      case 'DELETE':
        return this.httpClient.delete(url, { headers });
      case 'OPTIONS':
        return this.httpClient.get(url, { headers });
      default:
    }
  }
}

//{"email": "asd", "password": "pass", "display_name": "a s d"}
