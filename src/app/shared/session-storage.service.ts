import { Injectable } from '@angular/core';


@Injectable()
export class SessionStorageService {
  constructor() {
    // sessionStorage.clear();
  }

  public SetItem(key: string, value: any) {
    let val = JSON.stringify(value);
    sessionStorage.setItem(key, val);
  }
  public GetItem(key: string) {
    let val = sessionStorage.getItem(key);
    return JSON.parse(val);
  }
}


