FROM nginx

ADD dist/ /dist/

ADD nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
